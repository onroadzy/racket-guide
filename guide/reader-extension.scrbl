#lang scribble/doc
@(require scribble/manual scribble/bnf scribble/eval
          "guide-utils.rkt" "modfile.rkt"
          (for-label racket/match syntax/readerr))

@;{@title[#:tag "hash-reader"]{Reader Extensions}}
@title[#:tag "hash-reader"]{读取器扩展}

@;{@refdetails["parse-reader"]{reader extensions}}
@refdetails["parse-reader"]{读取器扩展}

@;{The @tech{reader} layer of the Racket language can be extended through
the @racketmetafont{#reader} form. A reader extension is implemented
as a module that is named after @racketmetafont{#reader}. The module
exports functions that parse raw characters into a form to be consumed
by the @tech{expander} layer.}
Racket语言的@tech{读取器（reader）}层可以通过@racketmetafont{#reader}表进行扩展。读取器扩展实现为一个以@racketmetafont{#reader}命名的模块。该模块导出将原始字符解析为@tech{扩展器}层使用的表的函数。

@;{The syntax of @racketmetafont{#reader} is}
@racketmetafont{#reader}的语法是

@racketblock[@#,(BNF-seq @litchar{#reader} @nonterm{module-path} @nonterm{reader-specific})]

@;{where @nonterm{module-path} names a module that provides
@racketidfont{read} and @racketidfont{read-syntax} functions. The
@nonterm{reader-specific} part is a sequence of characters that is
parsed as determined by the @racketidfont{read} and
@racketidfont{read-syntax} functions from @nonterm{module-path}.}
其中，@nonterm{module-path}命名了一个模块，该模块提供@racketidfont{read}和@racketidfont{read-syntax}函数。@nonterm{reader-specific}部分是由@nonterm{module-path}中的@racketidfont{read}和@racketidfont{read-syntax}函数确定的字符序列。

@;{For example, suppose that file @filepath{five.rkt} contains}
例如，假设文件@filepath{five.rkt}包含

@racketmodfile["five.rkt"]

@;{Then, the program}
那么，程序

@racketmod[
racket/base

'(1 @#,(elem @racketmetafont{#reader} @racket["five.rkt"] @tt{23456} @racket[7]) 8)
]

@;{is equivalent to}
等价于

@racketmod[
racket/base

'(1 ("23456") 7 8)
]

@;{because the @racketidfont{read} and @racketidfont{read-syntax}
functions of @filepath{five.rkt} both read five characters from the
input stream and put them into a string and then a list. The reader
functions from @filepath{five.rkt} are not obliged to follow Racket
lexical conventions and treat the continuous sequence @litchar{234567}
as a single number. Since only the @litchar{23456} part is consumed by
@racketidfont{read} or @racketidfont{read-syntax}, the @litchar{7}
remains to be parsed in the usual Racket way. Similarly, the reader
functions from @filepath{five.rkt} are not obliged to ignore
whitespace, and}
因为@filepath{five.rkt}的@racketidfont{read}和@racketidfont{read-syntax}函数都从输入流中读取五个字符，并把它们放入字符串，然后放入列表。@filepath{five.rkt}中的读取器函数不必遵循Racket词法约定，将连续序列@litchar{234567}视为单个数字。由于只有@litchar{23456}部分被@racketidfont{read}或@racketidfont{read-syntax}使用，所以@litchar{7}仍然需要以通常的Racket方式进行解析。类似地，@filepath{five.rkt}中的读取器函数不必忽略空白，并且

@racketmod[
racket/base

'(1 @#,(elem @racketmetafont{#reader} @racket["five.rkt"] @hspace[1] @tt{2345} @racket[67]) 8)
]

@;{is equivalent to}
等价于

@racketmod[
racket/base

'(1 (" 2345") 67 8)
]

@;{since the first character immediately after @racket["five.rkt"] is a
space.}
因为紧跟@racket["five.rkt"]后面的第一个字符是空格。

@;{A @racketmetafont{#reader} form can be used in the @tech{REPL}, too:}
@tech{REPL}中也可以使用@racketmetafont{#reader}表：

@interaction[
(eval:alts '@#,(elem @racketmetafont{#reader}@racket["five.rkt"]@tt{abcde}) '#reader"five.rkt"abcde)
]

@; ----------------------------------------------------------------------

@;{@section{Source Locations}}
@section[#:tag "Source_Locations"]{源位置}

@;{The difference between @racketidfont{read} and
@racketidfont{read-syntax} is that @racketidfont{read} is meant to be
used for data while @racketidfont{read-syntax} is meant to be used to
parse programs. More precisely, the @racketidfont{read} function will
be used when the enclosing stream is being parsed by the Racket
@racket[read], and @racketidfont{read-syntax} is used when the
enclosing stream is being parsed by the Racket @racket[read-syntax]
function. Nothing requires @racketidfont{read} and
@racketidfont{read-syntax} to parse input in the same way, but making
them different would confuse programmers and tools.}
@racketidfont{read}和@racketidfont{read-syntax}的区别在于，@racketidfont{read}用于数据，而 @racketidfont{read-syntax}用于解析程序。更准确地说，当通过Racket的@racket[read]解析封闭流时，将使用@racketidfont{read}函数，当Racket的@racket[read-syntax]函数解析封闭流时，使用@racketidfont{read-syntax}。没有什么需要@racketidfont{read}和@racketidfont{read-syntax}用同样的方式解析输入，但使它们不同会混淆程序员和工具。

@;{The @racketidfont{read-syntax} function can return the same kind of
value as @racketidfont{read}, but it should normally return a
@tech{syntax object} that connects the parsed expression with source
locations. Unlike the @filepath{five.rkt} example, the
@racketidfont{read-syntax} function is typically implemented directly
to produce @tech{syntax objects}, and then @racketidfont{read} can use
@racketidfont{read-syntax} and strip away @tech{syntax object}
wrappers to produce a raw result.}
@racketidfont{read-syntax}函数可以返回与@racketidfont{read}相同类型的值，但它通常应该返回一个@tech{语法对象（syntax object）}，该对象将解析的表达式与源位置连接起来。与@filepath{five.rkt}示例不同，@racketidfont{read-syntax}函数通常直接实现以生成@tech{语法对象}，然后@racketidfont{read}可以使用@racketidfont{read-syntax}并去掉@tech{语法对象}包装来产生原始结果。

@;{The following @filepath{arith.rkt} module implements a reader to
parse simple infix arithmetic expressions into Racket forms. For
example, @litchar{1*2+3} parses into the Racket form @racket[(+ (* 1
2) 3)]. The supported operators are @litchar{+}, @litchar{-},
@litchar{*}, and @litchar{/}, while operands can be unsigned integers
or single-letter variables. The implementation uses
@racket[port-next-location] to obtain the current source location, and
it uses @racket[datum->syntax] to turn raw values into @tech{syntax
objects}.}
下面的@filepath{arith.rkt}模块实现了一个读取器，用于将简单的中缀算术表达式解析为Racket表。例如，@litchar{1*2+3}解析为Racket表@racket[(+ (* 1
2) 3)]。支持的运算符是@litchar{+}、@litchar{-}、@litchar{*}和@litchar{/}，而操作数可以是无符号整数或单字母变量。该实现使用@racket[port-next-location]获取当前源位置，并使用 @racket[datum->syntax]将原始值转换为@tech{语法对象}。

@racketmodfile["arith.rkt"]

@;{If the @filepath{arith.rkt} reader is used in an expression position,
then its parse result will be treated as a Racket expression. If it is
used in a quoted form, however, then it just produces a number or a
list:}
如果在表达式位置使用@filepath{arith.rkt}读取器，则其解析结果将被视为Racket表达式。但是，如果它以引号形式使用，那么它只生成一个数字或一个列表：

@interaction[
(eval:alts @#,(elem @racketmetafont{#reader}@racket["arith.rkt"]@hspace[1]@tt{1*2+3}) #reader"arith.rkt" 1*2+3 )
(eval:alts '@#,(elem @racketmetafont{#reader}@racket["arith.rkt"]@hspace[1]@tt{1*2+3}) '#reader"arith.rkt" 1*2+3 )
]

@;{The @filepath{arith.rkt} reader could also be used in positions that
make no sense. Since the @racketidfont{read-syntax} implementation
tracks source locations, syntax errors can at least refer to parts of
the input in terms of their original locations (at the beginning of
the error message):}
@filepath{arith.rkt}读取器也可以用于毫无意义的位置。由于@racketidfont{read-syntax}实现跟踪源位置，语法错误至少可以根据其原始位置（在错误消息的开头）引用部分输入：

@interaction[
(eval:alts (let @#,(elem @racketmetafont{#reader}@racket["arith.rkt"]@hspace[1]@tt{1*2+3}) 8)
           (eval (parameterize ([read-accept-reader #t])
                   (read-syntax 'repl (let ([p @open-input-string{(let #reader"arith.rkt" 1*2+3 8)}])
                                        (port-count-lines! p)
                                        p)))))
]

@; ----------------------------------------------------------------------

@;{@section[#:tag "readtable"]{Readtables}}
@section[#:tag "readtable"]{可读表}

@;{A reader extension's ability to parse input characters in an arbitrary
way can be powerful, but many cases of lexical extension call for a
less general but more composable approach. In much the same way that
the @tech{expander} level of Racket syntax can be extended through
@tech{macros}, the @tech{reader} level of Racket syntax can be
composably extended through a @deftech{readtable}.}
读取器扩展以任意方式解析输入字符的能力可能是很强大，但许多词汇扩展需要一种不太通用但更易于组合的方法。与@tech{扩展器}级别的Racket语法可以通过@tech{宏}扩展的方式大致相同，@tech{读取器}级别的Racket语法可以通过@deftech{可读表（readtable）}进行组合扩展。

@;{The Racket reader is a recursive-descent parser, and the
@tech{readtable} maps characters to parsing handlers. For example, the
default readtable maps @litchar{(} to a handler that recursively
parses subforms until it finds a @litchar{)}. The
@racket[current-readtable] @tech{parameter} determines the
@tech{readtable} that is used by @racket[read] or
@racket[read-syntax]. Rather than parsing raw characters directly, a
reader extension can install an extended @tech{readtable} and then
chain to @racket[read] or @racket[read-syntax].}
例如，默认的Racket读取器是递归下降的解析器，@tech{可读表}将字符映射到解析处理程序。例如，默认可读表将映射到一个处理程序，该处理程序递归解析子表，直到找到一个@litchar{)}。@racket[current-readtable]的@tech{参数}确定了@racket[read]或@racket[read-syntax]使用的@tech{可读表}。而不是直接解析原始字符，读取器扩展可以安装一个扩展的@tech{可读表}，然后链接到@racket[read]或@racket[read-syntax]。

@;{@guideother{See @secref["parameterize"] for an introduction to
@tech{parameters}.}}
@guideother{有关《@tech{parameters}》的介绍，请参见《@secref["parameterize"]》。}

@;{The @racket[make-readtable] function constructs a new @tech{readtable}
as an extension of an existing one. It accepts a sequence of
specifications in terms of a character, a type of mapping for the
character, and (for certain types of mappings) a parsing
procedure. For example, to extend the readtable so that @litchar{$}
can be used to start and end infix expressions, implement a
@racket[read-dollar] function and use:}
@racket[make-readtable]函数构造了一个新的@tech{可读表}，作为现有@tech{可读表}的扩展。它接受字符、字符映射类型和（特定类型的映射）解析程序方面的一系列规范。例如，要扩展可读表，以便@litchar{$}可以用于开始和结束中缀表达式，请实现一个@racket[read-dollar]函数并使用：

@racketblock[
(make-readtable (current-readtable)
                #\$ 'terminating-macro read-dollar)
]

@;{The protocol for @racket[read-dollar] requires the function to accept
different numbers of arguments depending on whether it is being used
in @racket[read] or @racket[read-syntax] mode. In @racket[read] mode,
the parser function is given two arguments: the character that
triggered the parser function and the input port that is being
read. In @racket[read-syntax] mode, the function must accept four
additional arguments that provide the source location of the
character.}
@racket[read-dollar]的协议要求函数接受不同数量的参数，这取决于它是否在@racket[read]模式下使用还是在@racket[read-syntax]模式下使用。在@racket[read]模式下，解析器函数有两个参数：触发解析器的字符和正在读取的输入端口。在@racket[read-syntax]模式下，函数必须接受四个额外的参数，以提供字符的源位置。

@;{The following @filepath{dollar.rkt} module defines a
@racket[read-dollar] function in terms of the @racketidfont{read} and
@racketidfont{read-syntax} functions provided by @filepath{arith.rkt},
and it puts @racket[read-dollar] together with new @racketidfont{read} and
@racketidfont{read-syntax} functions that install the readtable and
chain to Racket's @racket[read] or @racket[read-syntax]:}
下面的@filepath{dollar.rkt}模块根据被@filepath{arith.rkt}提供的@racketidfont{read}和@racketidfont{read-syntax}函数定义了一个@racket[read-dollar]函数，并将@racket[read-dollar]与新的@racketidfont{read}和@racketidfont{read-syntax}函数放在一起，这些函数安装了可读表e并将其链接到Racket的@racket[read]或@racket[read-syntax]：

@racketmodfile["dollar.rkt"]

@;{With this reader extension, a single @racketmetafont{#reader} can be
used at the beginning of an expression to enable multiple uses of
@litchar{$} that switch to infix arithmetic:}
使用此读取器扩展，可以在表达式的开头使用单个@racketmetafont{#reader}，以启用切换到中缀运算的@litchar{$}的多种用法：

@interaction[
(eval:alts @#,(elem @racketmetafont{#reader}@racket["dollar.rkt"]@hspace[1]
                    @racket[(let ([a @#,tt{$1*2+3$}] [b @#,tt{$5/6$}]) $a+b$)])
           #reader"dollar.rkt" (let ([a $1*2+3$] [b $5/6$]) $a+b$))
]
