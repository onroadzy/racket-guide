#lang scribble/manual
@(require (only-in xrepl/doc-utils [cmd xreplcmd])
          "guide-utils.rkt")

@(define xrepl-doc '(lib "xrepl/xrepl.scrbl"))

@;{@title[#:tag "cmdline-tools"]{Command-Line Tools}}
@title[#:tag "cmdline-tools"]{命令行工具}

@;{Racket provides, as part of its standard distribution, a number of
command-line tools that can make racketeering more pleasant.}
作为其标准发行版的一部分，Racket提供了许多命令行工具，这些工具可以使racket使用者更加愉快。

@; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
@include-section["compile.scrbl"] @; raco

@; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
@;{@section{Interactive evaluation}}
@section{交互式求值}

@;{The Racket REPL provides everything you expect from a modern interactive
environment. For example, it provides an @xreplcmd{enter} command to have a
REPL that runs in the context of a given module, and an @xreplcmd{edit} command
to invoke your editor (as specified by the @envvar{EDITOR} environment
variable) on the file you entered. A @xreplcmd{drracket} command makes it easy
to use your favorite editor to write code, and still have DrRacket at hand to
try things out.}
Racket REPL提供了你从现代交互环境中所期望的一切。例如，它提供了一个@xreplcmd{enter}命令以使REPL在给定模块的上下文中运行，并提供了一个@xreplcmd{edit}命令来调用你输入的文件上的编辑器（由@envvar{EDITOR}环境变量指定）。@xreplcmd{drracket}命令可以很容易地使用你最喜欢的编辑器编来编写代码，同时仍有DrRacket可以尝试。

@;{For more information, see @other-doc[xrepl-doc].}
有关详细信息，请参见@other-doc[xrepl-doc]。

@; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
@;{@section{Shell completion}}
@section{Shell补全}

@;{Shell auto-completion for @exec{bash} and @exec{zsh} is available in
@filepath{share/pkgs/shell-completion/racket-completion.bash} and
@filepath{share/pkgs/shell-completion/racket-completion.zsh},
respectively.}
@exec{bash}和@exec{zsh}的Shell自动完成功能分别在@filepath{share/pkgs/shell-completion/racket-completion.bash}和@filepath{share/pkgs/shell-completion/racket-completion.zsh}中提供。

@;{To enable it, just run the appropriate file from your @tt{.bashrc} or
your @tt{.zshrc}.}
要启用它，只需从@tt{.bashrc}或@tt{.zshrc}运行相应的文件。

@;{The @filepath{shell-completion} collection is only available in the Racket Full
distribution. The completion scripts are also available
@hyperlink["https://github.com/racket/shell-completion"]{online}.}
@filepath{shell-completion}集合仅在Racket Full发行版中可用。完成脚本也可在@hyperlink["https://github.com/racket/shell-completion"]{联机}。 