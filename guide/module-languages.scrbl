#lang scribble/doc
@(require scribble/manual scribble/eval "guide-utils.rkt" "modfile.rkt"
          (for-label racket/date))

@;{@title[#:tag "module-languages"]{Module Languages}}
@title[#:tag "module-languages"]{模块语言}

@;{When using the longhand @racket[module] form for writing modules, the
module path that is specified after the new module's name provides the
initial imports for the module. Since the initial-import module
determines even the most basic bindings that are available in a
module's body, such as @racket[require], the initial import can be
called a @deftech{module language}.}
当使用通常写法@racket[module]表来编写模块时，在新模块名称之后指定的模块路径将为模块提供初始导入。由于初始导入模块甚至决定了模块主体中可用的最基本绑定，例如@racket[require]，因此初始导入可以称为@deftech{模块语言（module language）}。

@;{The most common @tech{module languages} are @racketmodname[racket] or
@racketmodname[racket/base], but you can define your own
@tech{module language} by defining a suitable module. For example,
using @racket[provide] subforms like @racket[all-from-out],
@racket[except-out], and @racket[rename-out], you can add, remove, or
rename bindings from @racketmodname[racket] to produce a @tech{module
language} that is a variant of @racketmodname[racket]:}
最常见的@tech{模块语言}是@racketmodname[racket]或@racketmodname[racket/base]，但你可以通过定义合适的模块来定义自己的@tech{模块语言}。例如，使用@racket[provide]子表，如@racket[all-from-out]、@racket[except-out]和@racket[rename-out]，你可以添加、删除或重命名@racketmodname[racket]中的绑定，以生成@tech{模块语言}，它是@racketmodname[racket]}的变体：

@;{@guideother{@secref["module-syntax"] introduces the longhand
@racket[module] form.}}
《@secref["module-syntax"]》介绍了@racket[module]表的通常写法。

@interaction[
(module raquet racket
  (provide (except-out (all-from-out racket) lambda)
           (rename-out [lambda function])))
(module score 'raquet
  (map (function (points) (case points
                           [(0) "love"] [(1) "fifteen"]
                           [(2) "thirty"] [(3) "forty"]))
       (list 0 2)))
(require 'score)
]

@; ----------------------------------------
@;{@section[#:tag "implicit-forms"]{Implicit Form Bindings}}
@section[#:tag "implicit-forms"]{隐式表绑定}

@;{If you try to remove too much from @racketmodname[racket] in defining
your own @tech{module language}, then the resulting module
will no longer work right as a @tech{module language}:}
如果在定义自己@tech{模块语言}时试图从@racketmodname[racket]中删除太多内容  ，那么生成的模块将不再作为@tech{模块语言}正常工作：

@interaction[
(module just-lambda racket
  (provide lambda))
(module identity 'just-lambda
  (lambda (x) x))
]

@;{The @racket[#%module-begin] form is an implicit form that wraps the
body of a module. It must be provided by a module that is to be used
as @tech{module language}:}
@racket[#%module-begin]表是一种封装模块主体的隐式表。它必须由要用作@tech{模块语言}的模块提供：

@interaction[
(module just-lambda racket
  (provide lambda #%module-begin))
(module identity 'just-lambda
  (lambda (x) x))
(require 'identity)
]

@;{The other implicit forms provided by @racket[racket/base] are
@racket[#%app] for function calls, @racket[#%datum] for literals, and
@racket[#%top] for identifiers that have no binding:}
@racket[racket/base]提供的其它隐式表包括：用于函数调用的@racket[#%app]、用于文本的@racket[#%datum]和用于没有绑定的标识@racket[#%top]：

@interaction[
(module just-lambda racket
  (provide lambda #%module-begin
           (code:comment @#,t{@racketidfont{ten} needs these, too:})
           #%app #%datum))
(module ten 'just-lambda
  ((lambda (x) x) 10))
(require 'ten)
]

@;{Implicit forms such as @racket[#%app] can be used explicitly in a module,
but they exist mainly to allow a module language to restrict or change
the meaning of implicit uses. For example, a @racket[lambda-calculus]
@tech{module language} might restrict functions to a single argument,
restrict function calls to supply a single argument, restrict the
module body to a single expression, disallow literals, and treat
unbound identifiers as uninterpreted symbols:}
隐式表，如@racket[#%app]可以在一个模块中显式地使用，但它们的存在主要是为了允许模块语言限制或更改隐式使用的含义。例如，@racket[lambda-calculus]@tech{模块语言}可能会将函数限制为单个参数，限制函数调用以提供单个参数，将模块主体限制为单个表达式，禁止使用文本，并将未绑定标识视为未解释的符号：

@interaction[
(module lambda-calculus racket
  (provide (rename-out [1-arg-lambda lambda]
                       [1-arg-app #%app]
                       [1-form-module-begin #%module-begin]
                       [no-literals #%datum]
                       [unbound-as-quoted #%top]))
  (define-syntax-rule (1-arg-lambda (x) expr)
    (lambda (x) expr))
  (define-syntax-rule (1-arg-app e1 e2)
    (#%app e1 e2))
  (define-syntax-rule (1-form-module-begin e)
    (#%module-begin e))
  (define-syntax (no-literals stx)
    (raise-syntax-error #f "no" stx))
  (define-syntax-rule (unbound-as-quoted . id)
    'id))
(module ok 'lambda-calculus
  ((lambda (x) (x z))
   (lambda (y) y)))
(require 'ok)
(module not-ok 'lambda-calculus
  (lambda (x y) x))
(module not-ok 'lambda-calculus
  (lambda (x) x)
  (lambda (y) (y y)))
(module not-ok 'lambda-calculus
  (lambda (x) (x x x)))
(module not-ok 'lambda-calculus
  10)
]

@;{Module languages rarely redefine @racket[#%app], @racket[#%datum], and
@racket[#%top], but redefining @racket[#%module-begin] is more
frequently useful. For example, when using modules to construct
descriptions of HTML pages where a description is exported from the
module as @racketidfont{page}, an alternate @racket[#%module-begin]
can help eliminate @racket[provide] and quasiquoting
boilerplate, as in @filepath{html.rkt}:}
模块语言很少重新定义@racket[#%app]、@racket[#%datum]和@racket[#%top]，但重新定义@racket[#%module-begin]往往更为有用。例如，当使用模块构建HTML页面的描述时，如果描述从模块导出为@racketidfont{页（page）}，那么另一个@racket[#%module-begin]可以帮助消除@racket[provide]和准引用样板，就像在@filepath{html.rkt}所示：

@racketmodfile["html.rkt"]

@;{Using the @filepath{html.rkt} @tech{module language}, a simple web page
can be described without having to explicitly define or export
@racketidfont{page} and starting in @racket[quasiquote]d mode instead
of expression mode:}
使用@filepath{html.rkt}@tech{模块语}，可以描述一个简单的网页，而无需显式定义或导出@racketidfont{页}，并以@racket[quasiquote]模式而不是表达式模式开始：

@interaction[
(module lady-with-the-spinning-head "html.rkt"
  (title "Queen of Diamonds")
  (p "Updated: " ,(now)))
(require 'lady-with-the-spinning-head)
page
]

@; ----------------------------------------
@;{@section[#:tag "s-exp"]{Using @racket[@#,hash-lang[] @#,racketmodname[s-exp]]}}
@section[#:tag "s-exp"]{使用@racket[@#,hash-lang[] @#,racketmodname[s-exp]]}

@;{Implementing a language at the level of @hash-lang[] is more complex
than declaring a single module, because @hash-lang[] lets programmers
control several different facets of a language. The
@racketmodname[s-exp] language, however, acts as a kind of
meta-language for using a @tech{module language} with the
@hash-lang[] shorthand:}
在@hash-lang[]级别实现语言比声明单个模块更复杂，因为@hash-lang[]允许程序员控制语言的多个不同方面。然而，@racketmodname[s-exp]语言作为一种元语言，使用带@hash-lang[]简写的@tech{模块语言}：

@racketmod[
s-exp _module-name
_form ...]

@;{is the same as}
等同于

@racketblock[
(module _name _module-name
  _form ...)
]

@;{where @racket[_name] is derived from the source file containing the
@hash-lang[] program. The name @racketmodname[s-exp] is short for
``@as-index{S-expression},'' which is a traditional name for
Racket's @tech{reader}-level lexical conventions: parentheses,
identifiers, numbers, double-quoted strings with certain backslash
escapes, and so on.}
其中@racket[_name]源自包含@hash-lang[]程序的源文件。名称@racketmodname[s-exp]是@as-index{S-expression}的缩写，它是@tech{读取器}级词汇约定的传统名称：括号、标识、数字、带反斜杠转义的双引号字符串等等。

@;{Using @racket[@#,hash-lang[] @#,racketmodname[s-exp]], the
@racket[lady-with-the-spinning-head] example from before can be
written more compactly as:}
使用@racket[@#,hash-lang[] @#,racketmodname[s-exp]]，前面的@racket[lady-with-the-spinning-head]例子可以写得更简洁：

@racketmod[
s-exp "html.rkt"

(title "Queen of Diamonds")
(p "Updated: " ,(now))
]

@;{Later in this guide, @secref["hash-languages"] explains how to define
your own @hash-lang[] language, but first we explain how you can write
@tech{reader}-level extensions to Racket.}
在这个指南的稍后边，《@secref["hash-languages"]》会讲解如何定义自己的@hash-lang[]语言，但是首先我们讲解你如何写针对Racket@tech{读取器（reader）}级的扩展。