# Racket编程指南

#### 介绍
本指南适用于新接触Racket的程序员或部分了解Racket的程序员。
为了更多的喜欢Racket的朋友更好的学习Racket，特对其进行翻译。

#### 软件架构明

本文档为Racket Scribble编写。

#### 安装教程

1.  通过Racket编译后可直接使用。

#### 使用说明

1.  本文档来自Racket6.12自带说明文档，经翻译而成。如对翻译内容有异议，以原文档为准。

#### 参与贡献

1.  翻译：OnRoadZy
2.  联系邮箱：amway_zhy@163.com
3.  微信：kangzi_great


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
