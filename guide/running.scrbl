#lang scribble/doc
@(require scribble/manual "guide-utils.rkt" (for-syntax racket/pretty))

@;{@title[#:tag "running" #:style 'toc]{Running and Creating Executables}}
@title[#:tag "running" #:style 'toc]{运行和创建可执行文件}

@;{While developing programs, many Racket programmers use the
@seclink["top" #:doc '(lib "scribblings/drracket/drracket.scrbl")
#:indirect? #t]{DrRacket} programming environment. To run a program without the
development environment, use @exec{racket} (for console-based
programs) or @exec{gracket} (for GUI programs). This chapter mainly
explains how to run @exec{racket} and @exec{gracket}.}
在开发程序时，很多Racket程序员使用@seclink["top" #:doc '(lib "scribblings/drracket/drracket.scrbl")
#:indirect? #t]{DrRacket}编程环境。要在没有开发环境的情况下运行程序，请使用@exec{racket}（用于基于控制台的程序）或@exec{gracket}（对于GUI程序）。本章主要介绍如何运行@exec{racket}和@exec{gracket}。

@local-table-of-contents[]

@; ----------------------------------------------------------------------

@;{@section[#:tag "racket"]{Running @exec{racket} and @exec{gracket}}}
@section[#:tag "racket"]{运行@exec{racket}和@exec{gracket}}

@;{The @exec{gracket} executable is the same as @exec{racket}, but with
small adjustments to behave as a GUI application rather than a console
application. For example, @exec{gracket} by default runs in
interactive mode with a GUI window instead of a console prompt. GUI
applications can be run with plain @exec{racket}, however.}
@exec{gracket}可执行文件和@exec{racket}一样，但表现上有小的调整，可作为GUI应用程序而不是控制台应用程序。例如，@exec{gracket}在默认情况下以交互模式运行，使用GUI窗口而不是控制台提示符。然而，GUI应用程序可以以普通的@exec{racket}运行。

@;{Depending on command-line arguments, @exec{racket} or @exec{gracket}
runs in @seclink["start-interactive-mode"]{interactive mode},
@seclink["start-module-mode"]{module mode}, or
@seclink["start-load-mode"]{load mode}.}
根据命令行参数的不同，@exec{racket}或@exec{gracket}在@seclink["start-interactive-mode"]{交互模式}、@seclink["start-module-mode"]{模块模式}或@seclink["start-load-mode"]{加载模式}下。

@;{@subsection[#:tag "start-interactive-mode"]{Interactive Mode}}
@subsection[#:tag "start-interactive-mode"]{交互模式}

@;{When @exec{racket} is run with no command-line arguments (other than
confguration options, like @Flag{j}), then it starts a @tech{REPL}
with a @litchar{> } prompt:}
当@exec{racket}在没有命令行参数的情况下运行时（除了配置选项，如@Flag{j}），那么启动@tech{REPL}使用@litchar{> }提示符：

@verbatim[#:indent 2]{
  @(regexp-replace #rx"\n+$" (banner) "")
  > 
}

@;{@margin-note{For enhancing your @tech{REPL} experience, see
  @racketmodname[xrepl]; for information on GNU Readline support, see
  @racketmodname[readline].}}
@margin-note{为了增强你的@tech{REPL}体验，请参见《@racketmodname[xrepl]》；有关GNU Readline支持的信息，请参见《@racketmodname[readline]》。}

@;{To initialize the @tech{REPL}'s environment, @exec{racket} first
requires the @racketmodname[racket/init] module, which provides all of
@racket[racket], and also installs @racket[pretty-print] for display
results. Finally, @exec{racket} loads the file reported by
@racket[(find-system-path 'init-file)], if it exists, before starting
the @tech{REPL}.}
要初始化@tech{REPL}的环境，@exec{racket}首先需要@racketmodname[racket/init]模块，该模块提供所有@racket[racket]，并安装@racket[pretty-print]以显示结果。最后，在启动@tech{REPL}之前，@exec{racket}加载@racket[(find-system-path 'init-file)]报告的文件（如果存在）。

@;{If any command-line arguments are provided (other than configuration
options), add @Flag{i} or @DFlag{repl} to re-enable the
@tech{REPL}. For example,}
如果提供了任何命令行参数（配置项除外），请添加@Flag{i}或@DFlag{repl}以重启REPL。例如,

@commandline{racket -e '(display "hi\n")' -i}

@;{displays ``hi'' on start-up, but still presents a @tech{REPL}.}
在启动时显示“hi”，但仍显示@tech{REPL}。

@;{If module-requiring flags appear before @Flag{i}/@DFlag{repl}, they
cancel the automatic requiring of @racketmodname[racket/init]. This
behavior can be used to initialize the @tech{REPL}'s environment with
a different language. For example,}
如果需要标志的模块出现在@Flag{i}/@DFlag{repl}之前，它们将取消对@racketmodname[racket/init]的自动需求。这种行为可用来使用其它语言初始化@tech{REPL}的环境。例如，

@commandline{racket -l racket/base -i}

@;{starts a @tech{REPL} using a much smaller initial language (that loads
much faster). Beware that most modules do not provide the basic syntax
of Racket, including function-call syntax and @racket[require]. For
example,}
使用更小的初始语言（加载速度更快）启动@tech{REPL}。请注意，大多数模块都不提供Racket的基本语法，包括函数调用语法和@racket[require]。例如,

@commandline{racket -l racket/date -i}

@;{produces a @tech{REPL} that fails for every expression, because
@racketmodname[racket/date] provides only a few functions, and not the
@racket[#%top-interaction] and @racket[#%app] bindings that are needed
to evaluate top-level function calls in the @tech{REPL}.}
生成一个对每个表达式都失败的@tech{REPL}，因为@racketmodname[racket/date]只提供了几个函数，而不是@tech{REPL}里需要求值顶层函数调用所需的@racket[#%top-interaction]和@racket[#%app]绑定。

@;{If a module-requiring flag appears after @Flag{i}/@DFlag{repl} instead
of before it, then the module is required after
@racketmodname[racket/init] to augment the initial environment. For
example,}
如果一个需求标志的模块出现在@Flag{i}/@DFlag{repl}之后，而不是出现在它之前，那么该模块需要在@racketmodname[racket/init]之后，以增强初始环境。例如，

@commandline{racket -i -l racket/date}

@;{starts a useful @tech{REPL} with @racketmodname[racket/date] available
in addition to the exports of @racketmodname[racket].}
除了@racketmodname[racket]的导出之外，可以使用@racketmodname[racket/date]启动一个有用的@tech{REPL}。

@; ----------------------------------------

@;{@subsection[#:tag "start-module-mode"]{Module Mode}}
@subsection[#:tag "start-module-mode"]{模块模式}

@;{If a file argument is supplied to @exec{racket} before any
command-line switch (other than configuration options), then the file
is required as a module, and (unless @Flag{i}/@DFlag{repl} is
specified), no @tech{REPL} is started. For example,}
如果一个文件参数在任何命令行开关（除了其它配置选项）之前提供给@exec{racket}，那么这个文件作为一个模块导入，没有@tech{REPL}启动。例如,

@commandline{racket hello.rkt}

@;{requires the @filepath{hello.rkt} module and then exits. Any argument
after the file name, flag or otherwise, is preserved as a command-line
argument for use by the required module via
@racket[current-command-line-arguments].}
需求@filepath{hello.rkt}模块，然后退出。文件名、标志或其它内容之后的任何参数都作为命令行参数保存，以供所需的模块通过@racket[current-command-line-arguments]使用。

@;{If command-line flags are used, then the @Flag{u} or
@DFlag{require-script} flag can be used to explicitly require a file
as a module.  The @Flag{t} or @DFlag{require} flag is similar, except
that additional command-line flags are processed by @exec{racket},
instead of preserved for the required module. For example,}
如果使用命令行标志，则可以使用@Flag{u}或@DFlag{require-script}标志来显示地将文件作为模块。@Flag{t}或@DFlag{require}标志类似，只是额外的命令行标志由@exec{racket}处理，而不是为需求的模块保留。例如，

@commandline{racket -t hello.rkt -t goodbye.rkt}

@;{requires the @filepath{hello.rkt} module, then requires the
@filepath{goodbye.rkt} module, and then exits.}
需求@filepath{hello.rkt}模块，然后需求@filepath{goodbye.rkt}模块，再然后退出。

@;{The @Flag{l} or @DFlag{lib} flag is similar to
@Flag{t}/@DFlag{require}, but it requires a module using a
@racket[lib] module path instead of a file path. For example,}
@Flag{l}或@DFlag{lib}标志类似于@Flag{t}/@DFlag{require}，但它需求一个使用@racket[lib]模块路径而不是文件路径的模块。例如，

@commandline{racket -l raco}

@;{is the same as running the @exec{raco} executable with no arguments,
since the @racket[raco] module is the executable's main module.}
与运行不带参数的@exec{raco}可执行文件是一样的，因为@racket[raco]模块是可执行文件的主模块。

@;{Note that if you wanted to pass command-line flags to
@racket[raco] above, you would need to protect the flags with a
@Flag{-}, so that @exec{racket} doesn't try to parse them itself:}
请注意，如果你想将命令行标志传递给上面的@racket[raco]，你需要用@Flag{-}保护这些标志，这样@exec{racket}就不会试图自己解析它们：

@commandline{racket -l raco -- --help}

@; ----------------------------------------

@;{@subsection[#:tag "start-load-mode"]{Load Mode}}
@subsection[#:tag "start-load-mode"]{加载模式}

@;{The @Flag{f} or @DFlag{load} flag supports @racket[load]ing top-level
expressions in a file directly, as opposed to expressions within a
module file. This evaluation is like starting a @tech{REPL} and typing
the expressions directly, except that the results are not printed.
For example,}
@Flag{f}或@DFlag{load}标志直接支持文件中的@racket[load]顶级表达式，而不是模块文件中的表达式。这个求值就像启动一个@tech{REPL}并直接键入表达式，只是结果不打印。例如，

@commandline{racket -f hi.rkts}

@;{@racket[load]s @filepath{hi.rkts} and exits. Note that load mode is
generally a bad idea, for the reasons explained in
@secref["use-module"]; using module mode is typically better.}
@racket[load]  @filepath{hi.rkts}并退出。请注意，由于@secref["use-module"]中解释的原因，加载模式通常是一个坏主意；使用模块模式通常更好。

@;{The @Flag{e} or @DFlag{eval} flag accepts an expression to evaluate
directly. Unlike file loading, the result of the expression is
printed, as in a @tech{REPL}. For example,}
@Flag{e}或@DFlag{eval}标志接受表达式直接求值。与文件加载不同，表达式的结果是打印的，就像在@tech{REPL}中一样。例如，

@commandline{racket -e '(current-seconds)'}

@;{prints the number of seconds since January 1, 1970.}
打印自1970年1月1日以来的秒数。

@;{For file loading and expression evaluation, the top-level environment
is created in the same way for
@seclink["start-interactive-mode"]{interactive mode}:
@racketmodname[racket/init] is required unless another module is
specified first. For example,}
对于文件加载和表达式求值，顶级环境的创建方式与@seclink["start-interactive-mode"]{交互模式}相同：除非首先指定了另一个模块，否则需求@racketmodname[racket/init]。例如,

@commandline{racket -l racket/base -e '(current-seconds)'}

@;{likely runs faster, because it initializes the environment for
evaluation using the smaller @racketmodname[racket/base] language,
instead of @racketmodname[racket/init].}
很可能运行得更快，因为它使用较小的@racketmodname[racket/base]语言而不是@racketmodname[racket/init]初始化求值环境。

@; ----------------------------------------------------------------------

@include-section["scripts.scrbl"]

@; ----------------------------------------------------------------------

@;{@section[#:tag "exe"]{Creating Stand-Alone Executables}}
@section[#:tag "exe"]{创建独立可执行文件}

@(define raco-doc '(lib "scribblings/raco/raco.scrbl"))

@;{For information on creating and distributing executables, see
@secref[#:doc raco-doc "exe"] and @secref[#:doc raco-doc "exe-dist"] in
@other-manual[raco-doc].}
有关创建和分发可执行文件的信息，参见在《@other-manual[raco-doc]》中的@secref[#:doc raco-doc "exe"]》和《@secref[#:doc raco-doc "exe-dist"]》。

@other-manual[raco-doc].