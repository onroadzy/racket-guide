#lang scribble/base
@(require scribble/manual
          "guide-utils.rkt")

@;{@title[#:tag "dialects" #:style 'toc]{Dialects of Racket and Scheme}}
@title[#:tag "dialects" #:style 'toc]{Racket和Scheme的方言}

@;{We use ``Racket'' to refer to a specific dialect of the Lisp language,
and one that is based on the Scheme branch of the Lisp family.
Despite Racket's similarity to Scheme, the @hash-lang[] prefix on
modules is a particular feature of Racket, and programs that start
with @hash-lang[] are unlikely to run in other implementations of
Scheme. At the same time, programs that do not start with @hash-lang[]
do not work with the default mode of most Racket tools.}
我们使用“Racket”来指Lisp语言的一种特定方言，它基于Lisp家族的Scheme分支。尽管Racket与Scheme相似，但模块上的@hash-lang[]前缀是Racket的一个特殊特性，以@hash-lang[]开始的程序不太可能在Scheme的其它实现中运行。同时，不以@hash-lang[]开始的程序不能在大多数Racket工具的默认模式下工作。

@;{``Racket'' is not, however, the only dialect of Lisp that is supported
by Racket tools. On the contrary, Racket tools are designed to support
multiple dialects of Lisp and even multiple languages, which allows
the Racket tool suite to serve multiple communities. Racket also gives
programmers and researchers the tools they need to explore and create
new languages.}
然而，“Racket”并不是Racket工具支持的唯一Lisp方言。相反，Racket工具旨在支持Lisp的多种方言，甚至是多种语言，这使得Racket工具包可以服务于多个社区。Racket还为程序员和研究人员提供了探索和创建新语言所需的工具。

@local-table-of-contents[]

@; --------------------------------------------------

@;{@section[#:tag "more-hash-lang"]{More Rackets}}
@section[#:tag "more-hash-lang"]{更多的Racket}

@;{``Racket'' is more of an idea about programming languages than a
language in the usual sense. Macros can extend a base language (as
described in @secref["macros"]), and alternate parsers can
construct an entirely new language from the ground up (as described in
@secref["languages"]).}
“Racket”更多的是关于编程语言的概念，而不是通常意义上的语言。宏可以扩展基本语言（如@secref["macros"]中所述），替代解析器可以从头构建一种全新的语言（如《@secref["languages"]》中所述）。

@;{The @hash-lang[] line that starts a Racket module declares the
base language of the module. By ``Racket,'' we usually mean
@hash-lang[] followed by the base language @racketmodname[racket] or
@racketmodname[racket/base] (of which @racketmodname[racket] is an
extension). The Racket distribution provides additional languages,
including the following:}
启动Racket模块的@hash-lang[]行声明了模块的基本语言。所谓“Racket”，我们通常指的是@hash-lang[]，后面是基础语言@racketmodname[racket]或@racketmodname[racket/base]（其中@racketmodname[racket]是一个扩展）。Racket发行版提供了其它语言，包括：

@itemize[

 @item{
  @;{@racketmodname[typed/racket] --- like
       @racketmodname[racket], but statically typed; see
       @other-manual['(lib "typed-racket/scribblings/ts-guide.scrbl")]}
@racketmodname[typed/racket]——像@racketmodname[racket]一样，但是静态类型的；参见《@other-manual['(lib "typed-racket/scribblings/ts-guide.scrbl")]》。
 }

 @item{
  @;{@racketmodname[lazy #:indirect] --- like
       @racketmodname[racket/base], but avoids evaluating an
       expression until its value is needed; see @seclink["top" #:doc
       '(lib "lazy/lazy.scrbl") #:indirect? #t]{the Lazy Racket
       documentation}.}
@racketmodname[lazy #:indirect]——像@racketmodname[racket/base]一样，但避免在需要表达式值之前对其求值；参见@seclink["top" #:doc
       '(lib "lazy/lazy.scrbl") #:indirect? #t]{惰性（Lazy）Racket文档}。
}

 @item{
  @;{@racketmodname[frtime #:indirect] --- changes evaluation in an
       even more radical way to support reactive programming; see
       @seclink["top" #:doc '(lib "frtime/scribblings/frtime.scrbl")
       #:indirect? #t]{the FrTime documentation}.}
@racketmodname[frtime #:indirect]——以更激进的方式改变求值，以支持反应式编程；参见@seclink["top" #:doc '(lib "frtime/scribblings/frtime.scrbl")
       #:indirect? #t]{FrTime文档}。 
    }
       
 @item{
  @;{@racketmodname[scribble/base] --- a language, which looks more
       like Latex than Racket, for writing documentation; see
       @other-manual['(lib "scribblings/scribble/scribble.scrbl")]}
@racketmodname[scribble/base]——一种看起来更像Latex而不是Racket的语言，用于编写文档；参见《@other-manual['(lib "scribblings/scribble/scribble.scrbl")]》。
 }

]

@;{Each of these languages is used by starting module with the language
name after @hash-lang[]. For example, this source of this
document starts with @racket[@#,hash-lang[] scribble/base].}
这些语言中的每一种都是通过在@hash-lang[]之后使用语言名称启动模块来使用的。例如，本文档的源码即以@racket[@#,hash-lang[] scribble/base]开头。

@;{Furthermore, Racket users can define their own languages, as discussed
in @secref["languages"]. Typically, a language name maps to its
implementation through a module path by adding
@racketidfont{/lang/reader}; for example, the language name
@racketmodname[scribble/base] is expanded to
@racket[scribble/base/lang/reader], which is the module that
implements the surface-syntax parser. Some language names act as
language loaders; for example, @racket[@#,hash-lang[]
@#,racketmodname[planet] _planet-path] downloads, installs, and uses a
language via @seclink["top" #:doc '(lib
"planet/planet.scrbl")]{@|PLaneT|}.}
此外，Racket用户可以定义自己的语言，如@secref["languages"]中所述。通常，一个语言名通过添加@racketidfont{/lang/reader}通过模块路径映射到它的实现；例如，语言名@racketmodname[scribble/base]扩展为@racket[scribble/base/lang/reader]，这是实现表面语法分析器的模块。一些语言名称充当语言加载程序；例如，@racket[@#,hash-lang[]
@#,racketmodname[planet] _planet-path]通过@seclink["top" #:doc '(lib
"planet/planet.scrbl")]{@|PLaneT|}下载、安装和使用一个语言。

@; --------------------------------------------------

@;{@section[#:tag "standards"]{Standards}}
@section[#:tag "standards"]{标准}

@;{Standard dialects of Scheme include the ones defined by @|r5rs| and
@|r6rs|.}
Scheme的标准方言包括由r5rs和r6rs定义的方言。

@;@subsection[#:tag "r5rs"]{@|r5rs|}}
@subsection[#:tag "r5rs"]{@|r5rs|}

@;{``@|r5rs|'' stands for @link["../r5rs/r5rs-std/index.html"]{The
Revised@superscript{5} Report on the Algorithmic Language Scheme}, and
it is currently the most widely implemented Scheme standard.}
“@|r5rs|”代表@link["../r5rs/r5rs-std/index.html"]{The
Revised@superscript{5} Report on the Algorithmic Language Scheme}，它是目前实施最广泛的Scheme标准。

@;{Racket tools in their default modes do not conform to @|r5rs|,
mainly because Racket tools generally expect modules, and @|r5rs|
does not define a module system. Typical single-file @|r5rs| programs
can be converted to Racket programs by prefixing them with
@racket[@#,hash-lang[] @#,racketmodname[r5rs]], but other Scheme
systems do not recognize @racket[@#,hash-lang[]
@#,racketmodname[r5rs]]. The @exec{plt-r5rs} executable (see
@secref[#:doc '(lib "r5rs/r5rs.scrbl") "plt-r5rs"]) more directly
conforms to the @|r5rs| standard.}
Racket工具在其默认模式下不符合@|r5rs|，这主要是因为Racket通常需要模块，而@|r5rs|没有定义模块系统。典型的单文件@|r5rs|程序可以通过在它们前面加上@racket[@#,hash-lang[] @#,racketmodname[r5rs]]来转换为Racket程序，但其它Scheme系统无法识别@racket[@#,hash-lang[]
@#,racketmodname[r5rs]]。@exec{plt-r5rs}可执行文件（参见《@secref[#:doc '(lib "r5rs/r5rs.scrbl") "plt-r5rs"]》）更直接地符合@|r5rs|标准。

@;{Aside from the module system, the syntactic forms and functions of
@|r5rs| and Racket differ. Only simple @|r5rs| become Racket
programs when prefixed with @racket[@#,hash-lang[] racket], and
relatively few Racket programs become @|r5rs| programs when a
@hash-lang[] line is removed. Also, when mixing ``@|r5rs| modules''
with Racket modules, beware that @|r5rs| pairs correspond to
Racket mutable pairs (as constructed with @racket[mcons]).}
除了模块系统之外，@|r5rs|和Racket的句法表和函数也有所不同。只有简单的@|r5rs|在前缀为@racket[@#,hash-lang[] racket]时才成为Racket程序，而当删除@hash-lang[]行时，相对较少的Racket编程成为@|r5rs|程序。此外，当将”@|r5rs|模块”与Racket模块混合时，请注意@|r5rs|序对对应于Racket可变序对（如用@racket[mcons]构造的）。

@;{See @other-manual['(lib "r5rs/r5rs.scrbl")] for more
information about running @|r5rs| programs with Racket.}
有关使用Racket运行@|r5rs|程序的详细信息，请参见《@other-manual['(lib "r5rs/r5rs.scrbl")]》。

@;{@subsection{@|r6rs|}}
@subsection[#:tag "r6rs"]{@|r6rs|}

@;{``@|r6rs|'' stands for @link["../r6rs/r6rs-std/index.html"]{The
Revised@superscript{6} Report on the Algorithmic Language Scheme},
which extends @|r5rs| with a module system that is similar to the
Racket module system.}
“@|r6rs|”代表@link["../r6rs/r6rs-std/index.html"]{The
Revised@superscript{6} Report on the Algorithmic Language Scheme}，该方案使用类似于Racket模块系统的模块系统扩展了@|r5rs|。

@;{When an @|r6rs| library or top-level program is prefixed with
@racketmetafont{#!}@racketmodname[r6rs] (which is valid @|r6rs|
syntax), then it can also be used as a Racket program. This works
because @racketmetafont{#!} in Racket is treated as a shorthand
for @hash-lang[] followed by a space, so
@racketmetafont{#!}@racketmodname[r6rs] selects the
@racketmodname[r6rs] module language. As with @|r5rs|, however, beware
that the syntactic forms and functions of @|r6rs| differ from
Racket, and @|r6rs| pairs are mutable pairs.}
当@|r6rs|库或顶级程序前缀为@racketmetafont{#!}时@racketmodname[r6rs]（有效的@|r6rs|语法），那么它也可以用作Racket程序。这是因为@racketmetafont{#!}在Racket中被视为@hash-lang[]的缩写，后跟空格，所以@racketmetafont{#!} @racketmodname[r6rs]选择@racketmodname[r6rs]模块语言。然而，与@|r5rs|一样，请注意@|r6rs|的语法表和函数与Racket不同，并且@|r6rs|序对是可变序对。

@;{See @other-manual['(lib "r6rs/scribblings/r6rs.scrbl")] for more
information about running @|r6rs| programs with Racket.}
参见@other-manual['(lib "r6rs/scribblings/r6rs.scrbl")]有关使用Racket运行@|r6rs|程序的详细信息，请参见《@other-manual['(lib "r6rs/scribblings/r6rs.scrbl")]》。

@; --------------------------------------------------

@;{@section[#:tag "teaching-langs"]{Teaching}}
@section[#:tag "teaching-langs"]{教学}

@;{The @|HtDP| textbook relies on pedagogic variants of Racket that
smooth the introduction of programming concepts for new programmers.
See @other-doc['(lib "scribblings/htdp-langs/htdp-langs.scrbl")
#:indirect @list{@|HtDP| language}].}
@|HtDP|教科书依靠Racket的教学变体，为新程序员顺利引入编程概念。请参见@other-doc['(lib "scribblings/htdp-langs/htdp-langs.scrbl")
#:indirect @list{@|HtDP|语言}]。

@;{The @|HtDP| languages are typically not used with @hash-lang[]
prefixes, but are instead used within DrRacket by selecting the
language from the @onscreen{Choose Language...} dialog.}
@|HtDP|语言通常不与@hash-lang[]前缀一起使用，而是通过从@onscreen{Choose Language...}对话框中选择语言在DrRacket中使用。