#lang scribble/doc
@(require scribble/manual "guide-utils.rkt")

@;{@title[#:tag "languages" #:style 'toc]{Creating Languages}}
@title[#:tag "languages" #:style 'toc]{创造语言}

@;{The @tech{macro} facilities defined in the preceding chapter let a
programmer define syntactic extensions to a language, but a macro is
limited in two ways:}
前一章中定义的@tech{宏}功能允许程序员定义语言的语法扩展，但宏有两种限制：

@itemlist[

 @;{@item{a macro cannot restrict the syntax available in its context or
       change the meaning of surrounding forms; and}}
   @item{宏不能限制上下文中可用的语法或改变包围的表的意义；}

 @;{@item{a macro can extend the syntax of a language only within the
       parameters of the language's lexical conventions, such as using
       parentheses to group the macro name with its subforms and using
       the core syntax of identifiers, keywords, and literals.}}
@item{宏只能在语言的词汇约定参数内扩展语言的语法，例如用括号将宏名称与其子表分组，以及用标识、关键字和核心语法。}

]

@;{@guideother{The distinction between the @tech{reader} and
@tech{expander} layer is introduced in @secref["lists-and-syntax"].}}
@guideother{@tech{读取器}和@tech{展开器}层之间的区别在《@secref["lists-and-syntax"]》中介绍。}

@;{That is, a macro can only extend a language, and it can do so only at
the @tech{expander} layer. Racket offers additional facilities for
defining a starting point of the @tech{expander} layer, for extending
the @tech{reader} layer, for defining the starting point of the
@tech{reader} layer, and for packaging a @tech{reader} and
@tech{expander} starting point into a conveniently named language.}
也就是说，宏只能扩展语言，并且只能在@tech{展开器}层进行扩展。Racket提供了额外的功能，用于定义@tech{展开器}层的起始点、@tech{读取器}层、定义@tech{读取器}层的起始点，以及将@tech{读取器}和@tech{展开器}起始点封装为方便命名的语言。

@local-table-of-contents[]

@;------------------------------------------------------------------------
@include-section["module-languages.scrbl"]
@include-section["reader-extension.scrbl"]
@include-section["hash-languages.scrbl"]
